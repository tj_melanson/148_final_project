#include "Hexa.h"
#include <vector>
#include <iostream>
using namespace Eigen;
using namespace Raytracer148;

void Hexahedron::findIntersection(HitRecord &result, Vector3d l0, Vector3d l, int face){

	double bias = 1e-4;
	Vector3d n = faces[face].normal;
	Vector3d p0 = c+ faces[face].offset*faces[face].normal;
	double num = (p0-l0).dot(n);	
	double denom = l.dot(n);
	
	if (denom < bias && denom > -bias) return;

	double t = num/denom;

	if (t < bias) return;

	Vector3d point = l0 + t*l;

	for (int i=0; i<6; i++){
		if (i == face) continue;
		Vector3d planeCenter = c + faces[i].offset * faces[i].normal;
		Vector3d planeOffset = (point - planeCenter).normalized();
		if (planeOffset.dot(faces[i].normal) > bias) return;
		
	}
	if (result.t < bias || t < result.t){
				result.t = t;
		result.position = point;
		result.normal = n;
	}
	
}


HitRecord Hexahedron::intersect(const Ray &ray){

	Vector3d rayDir = ray.direction.normalized();

	HitRecord result;
	result.normal.setZero(); result.position.setZero();
	result.t = -1;

	for (int i=0; i<6; i++){
		findIntersection(result, ray.origin, rayDir, i);
	}

	return result;
}

