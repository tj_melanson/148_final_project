#include "Triangle.h"
#include <iostream>
using namespace Eigen;
using namespace Raytracer148;

void Triangle::offset(Vector3d &disp)
{
    p1 = p1 + disp;
    p2 = p2 + disp;
    p3 = p3 + disp;
    center = center + disp;
}


void Triangle::cacheIntersectValues()
{
    d00 = e1.dot(e1);
    d01 = e1.dot(e3);
    d11 = e3.dot(e3);
    invDenom = 1.0 / (d00 * d11 - d01 * d01);

	r = (e1.norm() * e2.norm() * e3.norm())/(2*e1.cross(e2).norm()) ;
	double a = e1.norm();
	double t1 = sqrt(r*r - 0.25*a*a);

	//Found the bisector by just using the direction of the third point (no better way)
	Vector3d bisector = e1.cross(e2).cross(e1).normalized();
	center = p1 + e1/2 + t1*bisector;

	std::cout << "center: " << center.transpose() << std::endl;
	std::cout << "radius: " << r << std::endl;




}

// Compute barycentric coordinates (u, v, w) for
// point p with respect to triangle (a, b, c)
void Triangle::barycentric(Vector3d p, double &u, double &v, double &w)
{

    // Vector3d v0 = p2 - p1, v1 = p3 - p1;
    // double d00 = v0.dot(v0);
    // double d01 = v0.dot(v1);
    // double d11 = v1.dot(v1);
    // double invDenom = 1.0 / (d00 * d11 - d01 * d01);


    //Non-cached
    Vector3d es = p - p1;
    double d20 = es.dot(e1);
    double d21 = es.dot(e3);
    v = (d11 * d20 - d01 * d21) * invDenom;
    w = (d00 * d21 - d01 * d20) * invDenom;
    u = 1.0 - v - w;
}

HitRecord Triangle::intersect(const Ray &ray){
	

	//Bias for the rest of the calculations
	double bias = 1e-5;

	//Code written for clarity's sake. Optimizations can be done later

	HitRecord result;
	result.t = -1;

	//Ray components
	Vector3d l0 = ray.origin;
	Vector3d l = ray.direction.normalized();

	// result = boundingCircle->intersect(ray);

	// if (result.t < bias) return result;

    double demoninator = normal.dot(l);
    if (demoninator < bias) return result;
    Vector3d p0l0 = p1 - ray.origin; //Just the vector from the plane to origin, nothing else
    result.t = p0l0.dot(normal) / demoninator;

    if (result.t < bias) return result;

    result.position = ray.origin + result.t*l;

    Vector3d displacement = result.position - center;

    if (displacement.norm() - r > bias) {
        result.t = -1;
        return result;
    }

	double u,v,w;

    //Non-cached
    Vector3d es = result.position - p1;
    double d20 = es.dot(e1);
    double d21 = es.dot(e3);
    v = (d11 * d20 - d01 * d21) * invDenom;
    if (v < -bias || v > 1+bias){
    	result.t = -1;
    	return result;
    }
    w = (d00 * d21 - d01 * d20) * invDenom;
    u = 1.0 - v - w;

	if (u < -bias || w < -bias){ 
		result.t = -1;
	}

	result.normal = normal;
	return result;

/*
	//Calculate the triangle vectors
	Vector3d v1 = p2 - p1;
	Vector3d v2 = p3 - p1;

	//Find the offset from p1 to the ray origin
	Vector3d T = l0 - p1;

	//Compute the plane for e1 (P), e2 (Q)
	Vector3d P = l.cross(v2);
	Vector3d Q = T.cross(v1);

	//Find the determinant of the original matrix M
	double det = P.dot(v1);

	//If determinant is 0, then solution is non-unique / nonexistent, so return false intersect
	if (det < bias && det > -bias) 	return result;


	double a = 1/det;


	//Further weeding, find u,v via Cramer's Rule
	double u = a*P.dot(T);

	if (u<bias || u>1+bias) return result;

	double v = a*Q.dot(l);

	if (v<bias || u+v > 1+bias+bias) return result;

	//Find value for t -- component in Cramer's Rule to solve matrix
	double t = a * Q.dot(e2);

	result.t = t;
	result.position = l0 + t*l;
	result.normal = normal;


	return result;
*/
}
