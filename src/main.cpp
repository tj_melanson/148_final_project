
#define TINYOBJLOADER_IMPLEMENTATION // define this in only *one* .cc

#include <Image.h>
#include <Scene.h>
#include <Surface.h>
#include <Triangle.h>
#include <Mesh.h>

#include <tiny_obj_loader.h>
#include <iostream>
#include <vector>
#include <Eigen/Dense>
#include <Eigen/StdVector>

using namespace Eigen;
using namespace Raytracer148;
int main(int argc, char ** argv){


/*
 * Scene rendering
 */

Image im(1000, 1000);

Scene scene;

double reflection = 1.0;
double transparency = 1.0;

Vector3d surfaceColor(1,0,0);
Vector3d emissionColor(0,0,0);


/*
 * Obj parsing
 */
 
std::string inputfile = "cube.obj";
std::vector<tinyobj::shape_t> shapes;
std::vector<tinyobj::material_t> materials;



std::string err;
bool ret = tinyobj::LoadObj(shapes, materials, err, inputfile.c_str(), NULL, 3); //Put on the normal calculation flag

if (!err.empty()) { // `err` may contain warning message.
  std::cerr << err << std::endl;
}

if (!ret) {
  exit(1);
}

std::cout << "# of shapes     : " << shapes.size() << std::endl;
std::cout << "# of materials : " << materials.size() << std::endl;



for (size_t i = 0; i < shapes.size(); i++) {
  printf("shape[%ld].name = %s\n", i, shapes[i].name.c_str());
  printf("Size of shape[%ld].indices: %ld\n", i, shapes[i].mesh.indices.size());
  printf("Size of shape[%ld].material_ids: %ld\n", i, shapes[i].mesh.material_ids.size());
  assert((shapes[i].mesh.indices.size() % 3) == 0);
  for (size_t f = 0; f < shapes[i].mesh.indices.size() / 3; f++) {
    printf("  idx[%ld] = %d, %d, %d. mat_id = %d\n", f, shapes[i].mesh.indices[3*f+0], shapes[i].mesh.indices[3*f+1], shapes[i].mesh.indices[3*f+2], shapes[i].mesh.material_ids[f]);
  }

  printf("shape[%ld].vertices: %ld\n", i, shapes[i].mesh.positions.size());
  assert((shapes[i].mesh.positions.size() % 3) == 0);
  for (size_t v = 0; v < shapes[i].mesh.positions.size() / 3; v++) {
    printf("  v[%ld] = (%f, %f, %f)\n", v,
      shapes[i].mesh.positions[3*v+0],
      shapes[i].mesh.positions[3*v+1],
      shapes[i].mesh.positions[3*v+2]);
  }
}

for (size_t i = 0; i < materials.size(); i++) {
  printf("material[%ld].name = %s\n", i, materials[i].name.c_str());
  printf("  material.Ka = (%f, %f ,%f)\n", materials[i].ambient[0], materials[i].ambient[1], materials[i].ambient[2]);
  printf("  material.Kd = (%f, %f ,%f)\n", materials[i].diffuse[0], materials[i].diffuse[1], materials[i].diffuse[2]);
  printf("  material.Ks = (%f, %f ,%f)\n", materials[i].specular[0], materials[i].specular[1], materials[i].specular[2]);
  printf("  material.Tr = (%f, %f ,%f)\n", materials[i].transmittance[0], materials[i].transmittance[1], materials[i].transmittance[2]);
  printf("  material.Ke = (%f, %f ,%f)\n", materials[i].emission[0], materials[i].emission[1], materials[i].emission[2]);
  printf("  material.Ns = %f\n", materials[i].shininess);
  printf("  material.Ni = %f\n", materials[i].ior);
  printf("  material.dissolve = %f\n", materials[i].dissolve);
  printf("  material.illum = %d\n", materials[i].illum);
  printf("  material.map_Ka = %s\n", materials[i].ambient_texname.c_str());
  printf("  material.map_Kd = %s\n", materials[i].diffuse_texname.c_str());
  printf("  material.map_Ks = %s\n", materials[i].specular_texname.c_str());
  printf("  material.map_Ns = %s\n", materials[i].specular_highlight_texname.c_str());
  std::map<std::string, std::string>::const_iterator it(materials[i].unknown_parameter.begin());
  std::map<std::string, std::string>::const_iterator itEnd(materials[i].unknown_parameter.end());
  for (; it != itEnd; it++) {
    printf("  material.%s = %s\n", it->first.c_str(), it->second.c_str());
  }
  printf("\n");
}

//scene.addSurface (new Surface( new Circle(Vector3d(1,0,-1), Vector3d(0,0, 4), 3), surfaceColor, surfaceColor, surfaceColor, 0.0, 0.0));

/*
 * Adding obj to scene
 */

 
 
for (size_t i = 0; i < shapes.size(); i++) {
  assert((shapes[i].mesh.indices.size() % 3) == 0);
  std::vector<Triangle *> mesh_surface;
  for (size_t f = 0; f < shapes[i].mesh.indices.size() / 3; f++){
    int f_indices[3];
    std::vector<Vector3d,Eigen::aligned_allocator<Eigen::Vector3d> > points;
    Vector3d normal;
    for (int j = 0; j < 3; j++) {  
      f_indices[j] = shapes[i].mesh.indices[3*f+j];
      normal[j] = shapes[i].mesh.normals[3*f+j];
      Vector3d v;
      v <<  shapes[i].mesh.positions[3*f_indices[j]+0], shapes[i].mesh.positions[3*f_indices[j]+1], shapes[i].mesh.positions[3*f_indices[j]+2]; 
      v = v;
      v = v + Vector3d(0, 0, 3); //Need an offset to be visible
      points.push_back(v);

    }
    mesh_surface.push_back(new Triangle(points[0], points[1], points[2])); //, normal));
    std::cout << "triangle: " << points[0].transpose() << " " << points[1].transpose() << " " << points[2].transpose() << std::endl;
    std::cout << "normal: " << normal.transpose() << std::endl;

    //std::cout << "Mesh has " << mesh_surface.size() << " surfaces." << std::endl;
    int index = shapes[i].mesh.material_ids[f];
    std::cout << "Index: " << index << std::endl;
    //int first =  materials[index].diffuse[0]; // materials[index].ambient[0] + materials[index].diffuse[0] + materials[index].specular[0];
    //int second = materials[index].diffuse[1]; // materials[index].ambient[1] + materials[index].diffuse[1] + materials[index].specular[1];
    //int third =  materials[index].diffuse[2]; // materials[index].ambient[2] + materials[index].diffuse[2] + materials[index].specular[2];
    //Vector3d trueSurfaceColor(first, second, third);
    Vector3d ambient, diffuse, specular;
    for (int i=0; i<3; i++){
      ambient[i] = materials[index].ambient[i];
      diffuse[i] = materials[index].diffuse[i];
      specular[i] = materials[index].specular[i];
    }
    //std::cout << "surface color " << trueSurfaceColor.transpose() << std::endl;
    scene.addSurface(new Surface(new Triangle(points[0], points[1], points[2]), Vector3d(0,1,0), ambient, diffuse, specular, 0.0, 1.0));//trueSurfaceColor, emissionColor, 0.0, 0.0));
  }

}

std::cout << "rendering" << std::endl;
std::cout << "Scene shapes: " << scene.surfaces.size() << std::endl;


scene.render(im);

std::cout << "writing out" << std::endl;

im.writePNG("imageup.png");



return 0;





}
