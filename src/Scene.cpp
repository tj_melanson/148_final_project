#include "Scene.h"
#include <cmath>
#include <iostream>
using namespace Raytracer148;
using namespace std;
using namespace Eigen;

//[comment]
// This variable controls the maximum recursion depth
//[/comment]
#define MAX_RAY_DEPTH 5


float Scene::mix(const float &a, const float &b, const float &mix)
{
    return b * mix + a * (1 - mix);
}

//Need to find the shape corresponding to this ray
HitRecord Scene::closestHit(const Ray &ray, int &surfaceIndex) {
    HitRecord result;
    result.t = -1;
    bool foundSomething = false;



    for (unsigned int i = 0; i < surfaces.size(); i++) {
    	if (onlyInitial && initialSurfaces[i] == NULL) continue;
        HitRecord r = surfaces[i]->shape->intersect(ray);
        if (r.t > std::numeric_limits<double>::epsilon() && (!foundSomething || r.t < result.t)) {
            result = r;
            foundSomething = true;
	    surfaceIndex=i;
        }
    }
    return result;
}

Vector3d Scene::traceHelper(const Ray &ray, int depth, Eigen::Vector3d lightPosVec){
	int i = 0;
    HitRecord r = closestHit(ray, i);

    Vector3d result;
    result[0] = result[1] = result[2] = 0;

    if (r.t < std::numeric_limits<double>::epsilon()) return result; //numeric_limits<double>::epsilon()) return result;


    Vector3d lightDir = (lightPosVec - r.position).normalized();
    Vector3d surfaceColor = surfaces[i]->diffuseColor; //Change this to diffuseColor
    //r.position = phit
    //r.normal = nhit

    
    Surface *surface = surfaces[i]; //Retrieve intersecting shape from the closest hit
    float bias = 1e-4; // add some bias to the point from which we will be tracing
    bool inside = false;
    if (ray.direction.dot(r.normal) > 0) r.normal = -r.normal, inside = true; //ray is the same as rayorig, raydir

    Vector3d phit = r.position;
    Vector3d nhit = r.normal;
    Vector3d raydir = ray.direction;


    if ((surface->transparency > 0 || surface->reflection > 0) && depth < MAX_RAY_DEPTH) {
        float facingratio = -raydir.dot(nhit);
        // change the mix value to tweak the effect
        float fresneleffect = mix(pow(1 - facingratio, 3), 1, 0.2);
        // compute reflection direction (not need to normalize because all vectors
        // are already normalized)
        Vector3d refldir = raydir - nhit * 2 * raydir.dot(nhit);
        refldir.normalize();

	Ray reflRay;
        reflRay.origin = phit + nhit*bias;
  	reflRay.direction = refldir;

        Vector3d reflection = trace(reflRay, depth + 1);
        Vector3d refraction; refraction.setZero(3);
        // if the sphere is also transparent compute refraction ray (transmission)
        if (surface->transparency) {
            float ior = 1.1, eta = (inside) ? ior : 1 / ior; // are we inside or outside the surface?
            float cosi = -nhit.dot(raydir);
            float k = 1 - eta * eta * (1 - cosi * cosi);
            Vector3d refrdir = raydir * eta + nhit * (eta *  cosi - sqrt(k));
            refrdir.normalize();
	    Ray refrRay;
	    refrRay.origin = phit - nhit*bias;
	    refrRay.direction = refrdir;
            refraction = trace(refrRay, depth + 1);
        }
        // the result is a mix of reflection and refraction (if the sphere is transparent)
        surfaceColor =  (
            reflection * fresneleffect * surface->reflection +
            refraction * (1 - fresneleffect) * surface->transparency) + ((1-fresneleffect)*surface->reflection + fresneleffect*(surface->transparency))*surface->diffuseColor;	//Change this to diffuseColor
	
    }
    else {
        // it's a diffuse object, no need to raytrace any further


		// it's a diffuse object, no need to raytrace any further
		Vector3d lightDir  = (lightPosVec - r.position).normalized();
		Vector3d lightColor = { 1.0,1.0,1.0 };
		Vector3d reflDir = 2 *(lightDir.dot(r.normal)) * r.normal - lightDir;
		
		//Ambient component
		double   ambscale = 0.7f; 
		Vector3d ambient = ambscale * lightColor;

		//Diffuse Component
		double    diffscale = 1.0;
		double    diffmax = max((lightDir.dot(r.normal)), 0.0);
		Vector3d  diffuse = diffscale * diffmax * lightColor;

		//Specular component
		double    specscale = 0.5; // shape->specscale;
		double    specmax = pow(max((reflDir.dot(-ray.direction)), 0.0), 32);
		Vector3d  specular = specscale * specmax * lightColor;


	
		//Problem 3 - Shadows
		//If shading effect, check if item is in shadow
		//If yes, then clear light effect
    		Ray shadowray;
		shadowray.origin = r.position;
	        shadowray.direction =  lightDir;
		HitRecord shadowhit;
		for (int j = 0; j< surfaces.size(); j++)
			{
				if (i == j) continue;
				shadowhit = surfaces[j]->shape->intersect(shadowray);
				if (shadowhit.t > bias)
				   {
					//Also need to test if object is beyond light source
					if ((lightPosVec-r.position).norm() < (shadowhit.position - r.position).norm()+bias) continue;
					Vector3d lightDistance = lightPosVec-r.position;
					double distance = lightDistance.dot((shadowhit.position - r.position)) / (lightDistance.norm()*lightDistance.norm());
					diffuse  = distance * diffuse;
					specular = distance * specular;
					break;
				}
			}
		

		//Composite
		//surfaceColor = (ambient + diffuse + specular).cwiseProduct(surface->surfaceColor);
		surfaceColor = ambient.cwiseProduct(surface->ambientColor) + diffuse.cwiseProduct(surface->diffuseColor) + specular.cwiseProduct(surface->specularColor);
    }
    
    return surfaceColor;
}

// Returns an RGB color, where R/G/B are each in the range [0,1]
Vector3d Scene::trace(const Ray &ray, int depth) {
    Vector3d result;
    result[0] = result[1] = result[2] = 0;
    for(int cur_light = 0; cur_light < lightPoints.size(); cur_light ++){
        result += traceHelper(ray, depth, lightPoints[cur_light]);
    }

    return result/lightPoints.size();
}

void Scene::render(Image &image) {
    // We make the assumption that the camera is located at (0,0,0)
    // and that the image plane happens in the square from (-1,-1,1)
    // to (1,1,1).

    assert(image.getWidth() == image.getHeight());


    size_t  width = image.getWidth();
    size_t  height = image.getHeight();
    int size = width;
    double pixelSize = 2. / size;
    double invWidth = 1 / double(width), invHeight = 1 / double(height);
    double fov = 90.0, aspectratio = width / double(height);
	
    double angle = tan(M_PI * 0.5 * fov / 180.);

    //std::cout << "going into loop" << std::endl;

    for (int x = 0; x < size; x++)
        for (int y = 0; y < size; y++) {
			//For testing vector at a specific point
			//if (x != 115) continue;
			//if (y != 140) continue;

			Vector3d origin    = { 0,0,0 };
			Vector3d direction = { 0,0,0 };
			Vector3d color     = { 0,0,0 };

			//Use distributed ray tracing, based on stratified sampling
			// 9 cells about the current pixel 
			//for a total range of pixel +/- 0.5
			int cell_count = 9;
			for (int cell = 0; cell < cell_count; cell++)
			//-----------
			// for testing center of grid (same as single point)
			//int cell_count = 1;
			//for (int cell = 4; cell < 5; cell++)
			//
			{
				//calculate cell center location
				// x={-1/3, 0, 1/3}
				// y={-1/3, 0, 1/3}
				double cell_x = (cell % 3 - 1) / 3.0;
				double cell_y = (int (cell / 3) - 1) / 3.0; 

				//Calculate bias within the cell
				// x = -1/6 to 1/6
				// y = -1/6 to 1/6
				double bias_x = rand() / (3.0 * RAND_MAX) - 1.0 / 6.0;
				double bias_y = rand() / (3.0 * RAND_MAX) - 1.0 / 6.0;

				//Calculate based on pixel (1 pixel wide)
				// pixel_x = x + (0 to 1)
				double pixel_x = (x + 0.5) + cell_x + bias_x;
				double pixel_y = (y + 0.5) + cell_y + bias_y;
				
				//Now convert to ray value using aspect ratio and angle
				double ray_x = (2 * ((pixel_x) * invWidth) - 1) * angle * aspectratio;
				double ray_y = (1 - 2 * ((pixel_y) * invHeight)) * angle;
				double ray_z = 1;

				//Add adjustments to the x,y value to update the direction vector
				direction = {ray_x, ray_y,ray_z};

				Ray curRay;
			        curRay.origin = origin;
				curRay.direction =  direction;
				curRay.direction.normalize();
				color += trace(curRay,0) / (double) cell_count;
				
			}
			if (!onlyInitial || (color[0] || color[1] || color[2])) image.setColor(x, y, color[0], color[1], color[2]);
        }
}

void Scene::setInitial(int i){
	initialSurfaces[i] = surfaces[i];
	onlyInitial=true;
}
