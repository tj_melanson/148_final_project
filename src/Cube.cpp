#include "Cube.h"
#include <vector>
#include <iostream>
using namespace Eigen;
using namespace Raytracer148;

void Cube::findIntersection(HitRecord &result, Vector3d origin, Vector3d direction, float endComponent, float begComponent, float directionComponent, const Vector3d &normal){

	//Do this instead of dividing the delta by directionComponent (for t)
	Vector3d max, min;
	if (directionComponent < 0){
		max << xmin, ymin, zmin; 
		min << xmax, ymax, zmax;
	} else {
		max << xmax, ymax, zmax;
		min << xmin, ymin, zmin;
	}
	Vector3d offset = begComponent * direction;
	max = directionComponent * max + offset;
	min = directionComponent * min + offset;

 	Vector3d intersection = directionComponent*origin+endComponent*direction; 

	float bias=1e-4;
	for (int i=0; i<3; i++){
		//Don't do anything if invalid
		if (intersection[i] > max[i]+bias || intersection[i] < min[i]-2*bias) return;
	}
	float t = (endComponent-begComponent)/directionComponent; //Here division is unavoidable
	if (result.t <= 1e-4 || t < result.t){
		result.t = t;
		result.normal = normal;
		result.position = R.transpose()*intersection;
	}

}


HitRecord Cube::intersect(const Ray &ray){

	Vector3d rayDir = ray.direction.normalized();

	Ray localFrameRay;
	localFrameRay.origin = R*ray.origin;
	localFrameRay.direction = R*rayDir;

	float cosine = localFrameRay.direction.dot(rayDir);
	
	HitRecord result;
	result.position.setZero();
	result.normal.setZero();
	result.t = -1;

	findIntersection(result, localFrameRay.origin, localFrameRay.direction, xmin, localFrameRay.origin[0],  localFrameRay.direction[0], -x);
	findIntersection(result, localFrameRay.origin, localFrameRay.direction, xmax, localFrameRay.origin[0],  localFrameRay.direction[0],  x);
	findIntersection(result, localFrameRay.origin, localFrameRay.direction, ymin, localFrameRay.origin[1],  localFrameRay.direction[1], -y);
	findIntersection(result, localFrameRay.origin, localFrameRay.direction, ymax, localFrameRay.origin[1],  localFrameRay.direction[1],  y);
	findIntersection(result, localFrameRay.origin, localFrameRay.direction, zmin, localFrameRay.origin[2],  localFrameRay.direction[2], -z);
	findIntersection(result, localFrameRay.origin, localFrameRay.direction, zmax, localFrameRay.origin[2],  localFrameRay.direction[2],  z);

	return result;
}

