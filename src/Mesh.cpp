
#include "Mesh.h"

using namespace std;
using namespace Eigen;
using namespace Raytracer148;


void Mesh::offset(Vector3d &disp){
	for (auto face:faces){
		face->offset(disp);
	}

}

HitRecord Mesh::intersect(const Ray &ray){
	HitRecord result, cur;
	result.t = -1;
	for (auto face:faces){
		cur = face->intersect(ray);
		if (cur.t  > numeric_limits<double>::min() && (result.t < 0 || cur.t < result.t)) result = cur;
	}
	return result;
}

void Mesh::addTriangle(vector<Vector3d, aligned_allocator<Vector3d> > points, string line){
	stringstream ss;
	string type;
	ss >> type;
	int indices[3];
	ss >> indices[0] >> indices[1] >> indices[2];

	faces.push_back(new Triangle(points[indices[0]], points[indices[1]], points[indices[2]]) );
}

void Mesh::loadObj(string filename){

	vector<Vector3d, aligned_allocator<Vector3d> > points;
	ifstream objReader(filename);

	float x,y,z;
	string type;

	while ((objReader >> type) == "v" ){

	}
		
}
