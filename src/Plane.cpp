#include "Plane.h"
#include <limits>
#include <iostream>
using namespace Raytracer148;
using namespace Eigen;
using namespace std;

 
HitRecord Plane::intersect(const Ray &ray) {
     HitRecord result;
    result.t = -1;
    Vector3d l = ray.direction.normalized();
    

    // Following is psuedocody of what formula is. Haven't computer n yet.
    double demoninator = normal.dot(l);
    if(demoninator < -1e-6){
        result.position = p0 - ray.origin;
        result.t = result.position.dot(normal) / demoninator;
        if (result.t < 1e-6) result.t = -1;
        result.normal = normal;
    }

    return result;
}
