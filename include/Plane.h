#ifndef PLANE_H
#define PLANE_H

#include "Shape.h"

namespace Raytracer148 {
class Plane : public Shape {
public:
    Plane(Eigen::Vector3d n, Eigen::Vector3d c) : normal(n), p0(c){}

    virtual HitRecord intersect(const Ray &ray);

//private:
    Eigen::Vector3d normal, p0;
};
}

#endif
