#ifndef SCENE_H
#define SCENE_H

#include "Shape.h"
#include "Image.h"
#include "Surface.h"
#include <vector>
#include <map>
#include <limits>

namespace Raytracer148 {
class Scene {
public:
  Scene() {
    Eigen::Vector3d lightPos;
    Eigen::Vector3d lightPos_2;
    lightPos[0] = 0;
    lightPos[1] = 4;
    lightPos[2] = 0;
    lightPos_2[0] = 0;
    lightPos_2[1] = 2;
    lightPos_2[2] = 7;
    lightPoints.push_back(lightPos);
    lightPoints.push_back(lightPos_2);
    onlyInitial = false;
  }

  virtual ~Scene() {
    for (unsigned int i = 0; i < surfaces.size(); i++)
      delete surfaces[i];
    surfaces.resize(0);
  }


  void setInitial(int i);
  void addSurface(Surface *s) { surfaces.push_back(s); }
  HitRecord closestHit(const Ray &ray, int &surfaceIndex);
  Eigen::Vector3d traceHelper(const Ray &ray, int depth, Eigen::Vector3d lightPosVec);
  Eigen::Vector3d trace(const Ray &ray, int depth=0);
  void render(Image &image);
  float mix(const float &a, const float &b, const float &mix);
//private:
  std::vector<Surface*> surfaces;
  //Eigen::Vector3d lightPos;
  std::vector<Eigen::Vector3d> lightPoints;
  std::map<int, Surface*> initialSurfaces;
  bool onlyInitial;
};
}

#endif
