#ifndef SHAPE_H
#define SHAPE_H

#include <Eigen/Dense>
namespace Raytracer148 {
struct Ray {
  Eigen::Vector3d origin, direction;
};

class Shape;

struct HitRecord {
  Eigen::Vector3d position, normal;
  double t;
};

class Shape {
public:
  virtual ~Shape(){}
  virtual HitRecord intersect(const Ray &ray) = 0;
  virtual void offset(Eigen::Vector3d &disp) = 0; //new center is in local coordinates
};

}

#endif
