#ifndef MESH_H
#define MESH_H

#include "Shape.h"
#include "Triangle.h"
#include <iostream>
#include  <vector>
#include <Eigen/Dense>
#include <Eigen/StdVector>
#include <fstream>

using namespace Eigen;
using namespace std;

namespace Raytracer148 {
class Mesh: public Shape {
public:

	Mesh(){}
	Mesh(vector<Triangle *> mesh){
		faces = mesh;
		sort(faces.begin(), faces.end(), [](Triangle *t1, Triangle *t2){return (t1 && t2)? *t1 < *t2 : false;});
	}

        virtual void offset(Vector3d &disp);
	void addTriangle(vector<Vector3d, aligned_allocator<Vector3d> > points, string line);
	void loadObj(string file);
    virtual HitRecord intersect(const Ray &ray);

private:
	vector<Triangle *> faces;

};
}

#endif
