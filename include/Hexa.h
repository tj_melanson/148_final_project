#ifndef HEXA_H
#define HEXA_H

#include "Shape.h"
#include <iostream>
using namespace Eigen;
namespace Raytracer148 {


struct BoundingBox{
	Vector3d max;
	Vector3d min;
};

struct Plane{
	Vector3d normal;
	double offset;
};

class  Hexahedron: public Shape {
public:
    Hexahedron(Vector3d &center, Plane f[6]) : c(center){
	for (int i=0; i<6; i++){
		faces[i].normal = f[i].normal.normalized();
		faces[i].offset = f[i].offset;
	}

    }

    void findIntersection(HitRecord &result, Vector3d l0, Vector3d l, int face);
    virtual HitRecord intersect(const Ray &ray);

//private:
    Plane faces[6];
    Vector3d c;
    BoundingBox box;
};
}

#endif
