#ifndef CUBE_H
#define CUBE_H

#include "Shape.h"
#include <iostream>

namespace Raytracer148 {
class Cube : public Shape {
public:
    Cube(Eigen::Vector3d &center, float radius, Eigen::Vector3d xAxis = Eigen::Vector3d(1,0,0), Eigen::Vector3d yAxis = Eigen::Vector3d(0,1,0), Eigen::Vector3d zAxis = Eigen::Vector3d(0,0,1)) : c(center), h(radius) {
	x = xAxis.normalized();
	y = yAxis.normalized();
	z = zAxis.normalized();

	R << x.transpose(), y.transpose(), z.transpose();

	Eigen::Vector3d localFrameCenter = R*center;
	xmin = localFrameCenter[0] - h;
	xmax = localFrameCenter[0] + h;
	ymin = localFrameCenter[1] - h;
	ymax = localFrameCenter[1] + h;
	zmin = localFrameCenter[2] - h;
	zmax = localFrameCenter[2] + h;

    }

    void findIntersection(HitRecord &result, Eigen::Vector3d origin, Eigen::Vector3d direction, float endComponent, float begComponent, float directionComponent, const Eigen::Vector3d &normal);
    virtual HitRecord intersect(const Ray &ray);
    Eigen::Matrix3d R; //Converts world coordinates to cube coordinates
    float xmin, xmax;
    float ymin, ymax;
    float zmin, zmax;
private:
    Eigen::Vector3d c;
    float h; //NOTE: only half the length of a side
    Eigen::Vector3d x,y,z;

};
}

#endif
