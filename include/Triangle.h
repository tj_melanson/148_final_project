#ifndef TRI_H
#define TRI_H

#include "Shape.h"
#include <iostream>

using namespace Eigen;
using namespace std;

namespace Raytracer148 {
class Triangle: public Shape {
public:

    Triangle(){}
    Triangle(Vector3d &point1, Vector3d &point2, Vector3d &point3, Vector3d norm): p1(point1), p2(point2), p3(point3){
    	//Just set the normal vector to the provided vector
        e1 = point2 - point1;
        e2 = point3 - point2;
        e3 = point3 - point1;

    	normal = norm.normalized();

        cacheIntersectValues();

    }

    Triangle(Vector3d &point1, Vector3d &point2, Vector3d &point3): p1(point1), p2(point2), p3(point3){
	    //Use the value calculated assuming counterclockwise vertex order
		//TODO: make sure this is in the direction of the provided vector
		e1 = point2 - point1;
		e2 = point3 - point2;
        e3 = point3 - point1;

		normal = -(e1.cross(e2).normalized());

        cacheIntersectValues();
    }

    virtual void offset(Vector3d &disp);
    void cacheIntersectValues();
    void barycentric(Vector3d p, double &u, double &v, double &w);
    virtual HitRecord intersect(const Ray &ray);
    bool operator<(const Triangle &rhs) const{return min(min(p1[2], p2[2]), p3[2]) < min(min(rhs.p1[2], rhs.p2[2]), rhs.p3[2]);} //Lesser triangle are in front of greater
private:
    Vector3d normal, p1, p2, p3;
    Vector3d e1, e2, e3, center;
    double d00, d01, d11, r;
    double invDenom;
};
}

#endif
