#include "Shape.h"
#include <Eigen/Dense>

using namespace Eigen;
#ifndef SURFACE_H
#define SURFACE_H

namespace Raytracer148{

class Surface{

public:
	Shape *shape;
	Vector3d center;
	Vector3d surfaceColor;
	Vector3d ambientColor;
	Vector3d diffuseColor;
	Vector3d specularColor;
	Vector3d emissionColor;
	double transparency;
	double reflection;

	Surface(Shape *s, Vector3d sc, Vector3d ec, double t, double r){
		shape = s;
		surfaceColor = sc;
		emissionColor = ec;
		transparency = t;
		reflection = r;
	}		

	Surface(Shape *s, Vector3d c, Vector3d ambient, Vector3d diffuse, Vector3d specular, double t, double r): shape(s), center(c), ambientColor(ambient), 
																											diffuseColor(diffuse), specularColor(specular), transparency(t), reflection(r){ shape->offset(c);}
};
}
#endif
